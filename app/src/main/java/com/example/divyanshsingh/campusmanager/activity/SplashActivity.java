package com.example.divyanshsingh.campusmanager.activity;

import android.content.Intent;
import android.os.Trace;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.divyanshsingh.campusmanager.R;
import com.example.divyanshsingh.campusmanager.controller.AppController;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created By Divyansh Singh
 */

public class SplashActivity extends AppCompatActivity {
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Thread background = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                    Intent intent;
                    new AppController(SplashActivity.this);
                    if (mAuth == null || mAuth.getCurrentUser() == null) {
                        intent = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }else{
                        intent = new Intent(SplashActivity.this,DashboardActivity.class);
                        startActivity(intent);
                    }
                    finish();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        background.start();
    }
}
