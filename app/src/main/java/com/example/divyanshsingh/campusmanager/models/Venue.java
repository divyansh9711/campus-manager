package com.example.divyanshsingh.campusmanager.models;

import android.os.Parcel;
import android.os.Parcelable;
/**
 * Created By Divyansh Singh
 */

public class Venue implements Parcelable{


    private String name;
    private String addressId;     // HOSTEL - 1, ACAD - 2 , OTHER - 3
    private String room;          // Only for acads and hostels
    private String venueId;       // BH1 - 1, BH2 - 2, BH3 - 3, GH1 - 4, ACAD - 5, SAC - 6, OAT - 7, CP - 8, OTHER - 9
    private String createdBy;     // userId
    private String createdDate;   // Venue Create date
    private String modifiedDate;

    public Venue(){}

    protected Venue(Parcel in) {
        name = in.readString();
        addressId = in.readString();
        room = in.readString();
        venueId = in.readString();
        createdBy = in.readString();
        createdDate = in.readString();
        modifiedDate = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(addressId);
        dest.writeString(room);
        dest.writeString(venueId);
        dest.writeString(createdBy);
        dest.writeString(createdDate);
        dest.writeString(modifiedDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Venue> CREATOR = new Creator<Venue>() {
        @Override
        public Venue createFromParcel(Parcel in) {
            return new Venue(in);
        }

        @Override
        public Venue[] newArray(int size) {
            return new Venue[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getVenueId() {
        return venueId;
    }

    public void setVenueId(String venueId) {
        this.venueId = venueId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getVenueName(){
        //TODO this scheme is not correct. Returns GH when it should return ACAD
        // BH1 - 1, BH2 - 2, BH3 - 3, GH1 - 4, ACAD - 5, SAC - 6, OAT - 7, CP - 8, OTHER - 9
        switch (venueId){
            case "1" : return "BH1";
            case "2" : return "BH2";
            case "3" : return "BH3";
            case "4" : return "GH";
            case "5" : return "ACAD";
            case "6" : return "SAC";
            case "7" : return "OAT";
            case "8" : return "CP";
            case "9" : return "OTHER";
            default : return "OTHER";
        }
    }
}
