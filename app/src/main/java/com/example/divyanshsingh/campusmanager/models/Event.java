package com.example.divyanshsingh.campusmanager.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.ArrayList;

/**
 * Created By Divyansh Singh
 */
public class Event extends Profile implements Comparable<Event>{

    private Venue venue;           // Venue of the event
    private Timing timing;         // Timing of the event
    private Category audianceType;
    private ArrayList<String> interestList;
    public Event(){super();}


    protected Event(Parcel in) {
        super(in);
        venue = in.readParcelable(Venue.class.getClassLoader());
        timing = in.readParcelable(Timing.class.getClassLoader());
        audianceType = in.readParcelable(Category.class.getClassLoader());
        interestList = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest,flags);
        dest.writeParcelable(venue, flags);
        dest.writeParcelable(timing, flags);
        dest.writeParcelable(audianceType, flags);
        dest.writeStringList(interestList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    public ArrayList<String> getInterestList() {
        return interestList;
    }

    public void setInterestList(ArrayList<String> interestList) {
        this.interestList = interestList;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public Timing getTiming() {
        return timing;
    }

    public void setTiming(Timing timing) {
        this.timing = timing;
    }

    public Category getAudianceType() {
        return audianceType;
    }

    public void setAudianceType(Category audianceType) {
        this.audianceType = audianceType;
    }

    @Override
    public int compareTo(@NonNull Event o) {
        Long num =Long.parseLong(this.getCreateDateId()) - Long.parseLong(o.getCreateDateId());
        Integer ret = num.intValue();
        return ret;
    }


}
