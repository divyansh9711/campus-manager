package com.example.divyanshsingh.campusmanager.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.divyanshsingh.campusmanager.R;
import com.example.divyanshsingh.campusmanager.adapters.EventAdapter;
import com.example.divyanshsingh.campusmanager.controller.AppController;
import com.example.divyanshsingh.campusmanager.models.Event;
import com.example.divyanshsingh.campusmanager.utils.CommonProgressDialog;
import com.example.divyanshsingh.campusmanager.utils.Constants;
import com.example.divyanshsingh.campusmanager.utils.DateTimeUtils;
import com.example.divyanshsingh.campusmanager.utils.SingleButtonError;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;

/**
 * Created By Divyansh Singh
 */
public class DashboardActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView eventsRecycler;
    private EventAdapter eventAdapter;
    private TextView getRsvpEvents, getMyEvents, getAllEvents, addEventText, todayViewText, showAllText, logOut, userName, noEvents;
    private FirebaseDatabase database;
    private DatabaseReference reference;
    private ArrayList<Event> formalList, informalList, list, rsvpList, myEvents;
    private Dialog progressDialog;
    private DrawerLayout drawer;
    private boolean doubleBackToExitPressedOnce = false;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        eventsRecycler = findViewById(R.id.events_recycler);
        ImageButton addEvent = findViewById(R.id.add_event);
        ImageButton todayEvents = findViewById(R.id.todayView);
        ImageButton allEvents = findViewById(R.id.showAll);
        getRsvpEvents = findViewById(R.id.liked_events);
        getMyEvents = findViewById(R.id.my_events);
        logOut = findViewById(R.id.log_out);
        noEvents = findViewById(R.id.no_events);
        userName = findViewById(R.id.welcomeName);

        String firstName = "", lastName = "";
        String name = AppController.prefs.getString(Constants.loggedInUserName, "Curious One");
        int index = Objects.requireNonNull(name.indexOf(" "));
        firstName = Objects.requireNonNull(Objects.requireNonNull(name.substring(0, index)));
        lastName = Objects.requireNonNull(name.substring(index + 1, name.length()));
        firstName = firstName.toLowerCase();
        lastName = lastName.toLowerCase();
        String temp = String.valueOf(firstName.charAt(0)).toUpperCase();
        firstName = temp + firstName.substring(1, firstName.length());
        temp = String.valueOf(lastName.charAt(0)).toUpperCase();
        lastName = temp + lastName.substring(1, lastName.length());
        userName.setText(firstName + " " + lastName);

        getAllEvents = findViewById(R.id.all_events);
        addEventText = findViewById(R.id.add_event_text);
        todayViewText = findViewById(R.id.today_view_text);
        showAllText = findViewById(R.id.show_all_text);
        database = FirebaseDatabase.getInstance();
        reference = database.getReference("events");
        progressDialog = CommonProgressDialog.LoadingSpinner(DashboardActivity.this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, 1, false);
        eventsRecycler.setLayoutManager(linearLayoutManager);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));
        list = new ArrayList<>();
        formalList = new ArrayList<>();
        informalList = new ArrayList<>();
        rsvpList = new ArrayList<>();
        myEvents = new ArrayList<>();
        eventAdapter = new EventAdapter(DashboardActivity.this, list);
        eventsRecycler.setAdapter(eventAdapter);
        getEvents();


        logOut.setOnClickListener(this);
        getMyEvents.setOnClickListener(this);
        getRsvpEvents.setOnClickListener(this);
        getAllEvents.setOnClickListener(this);

        addEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashboardActivity.this, AddEventActivity.class);
                startActivity(intent);
            }
        });
        addEventText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashboardActivity.this, AddEventActivity.class);
                startActivity(intent);
            }
        });

        todayEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noEvents.setVisibility(View.GONE);
                eventsRecycler.setVisibility(View.VISIBLE);
                myEvents.clear();
                SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyy");
                Date c = Calendar.getInstance().getTime();
                String dateString = df.format(c);
                for (Event event : list) {
                    if (event.getTiming().getStartDate().equals(dateString) && event.getInterestList().contains(AppController.prefs.getString(Constants.loggedInUserId, ""))) {
                        myEvents.add(event);
                    }
                }
                if (myEvents.size() == 0) {
                    noEvents.setVisibility(View.VISIBLE);
                    eventsRecycler.setVisibility(View.GONE);
                }
                eventAdapter.addData(myEvents);
            }
        });
        todayViewText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noEvents.setVisibility(View.GONE);
                eventsRecycler.setVisibility(View.VISIBLE);
                myEvents.clear();
                SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyy");
                Date c = Calendar.getInstance().getTime();
                String dateString = df.format(c);
                for (Event event : list) {
                    if (event.getTiming().getStartDate().equals(dateString) && event.getInterestList().contains(AppController.prefs.getString(Constants.loggedInUserId, ""))) {
                        myEvents.add(event);
                    }
                }
                if (myEvents.size() == 0) {
                    noEvents.setVisibility(View.VISIBLE);
                    eventsRecycler.setVisibility(View.GONE);
                }
                eventAdapter.addData(myEvents);
            }
        });

        allEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventsRecycler.setVisibility(View.VISIBLE);
                eventAdapter.addData(list);
            }
        });
        showAllText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventsRecycler.setVisibility(View.VISIBLE);
                eventAdapter.addData(list);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }


    private void getEvents() {
        noEvents.setVisibility(View.GONE);
        progressDialog.show();
        formalList.clear();
        informalList.clear();
        list.clear();
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                formalList.clear();
                informalList.clear();
                noEvents.setVisibility(View.GONE);
                eventsRecycler.setVisibility(View.VISIBLE);
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    for (DataSnapshot shot : postSnapshot.getChildren()) {
                        Event event = shot.getValue(Event.class);

                        if (Long.parseLong(shot.getKey().substring(0, 7)) >= Long.parseLong(DateTimeUtils.getCurrentDateId().substring(0, 7))) {
                            assert event != null;
                            if (event.getType().getCategoryId().equals("0")) {
                                formalList.add(event);
                            } else {
                                informalList.add(event);
                            }
                        }
                    }
                }
                Collections.sort(formalList);
                Collections.sort(informalList);
                list.clear();
                list.addAll(formalList);
                list.addAll(informalList);
                rsvpList.clear();
                for (Event event : list) {
                    if (event.getInterestList().contains(AppController.prefs.getString(Constants.loggedInUserId, "")) && event.getInterestList().contains(AppController.prefs.getString(Constants.loggedInUserId, ""))) {
                        rsvpList.add(event);
                    }
                }
                eventAdapter.addData(rsvpList);
                if (rsvpList.size() == 0) {
                    eventsRecycler.setVisibility(View.GONE);
                    noEvents.setVisibility(View.VISIBLE);
                }

                //eventAdapter.addData(list);
//                list.clear();
//                formalList.clear();
//                informalList.clear();
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressDialog.dismiss();
                SingleButtonError.with(DashboardActivity.this)
                        .setMessage(Objects.requireNonNull(databaseError.getMessage()))
                        .hideHeading()
                        .show();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.all_events:
                eventAdapter.addData(list);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                break;
            case R.id.my_events:
                noEvents.setVisibility(View.GONE);
                eventsRecycler.setVisibility(View.VISIBLE);
                myEvents.clear();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                for (Event event : list) {
                    if (event.getCreator().getCreatorId().equals(AppController.prefs.getString(Constants.loggedInUserId, ""))) {
                        myEvents.add(event);
                    }
                }
                eventAdapter.addData(myEvents);
                if (myEvents.size() == 0) {
                    noEvents.setVisibility(View.VISIBLE);
                    eventsRecycler.setVisibility(View.GONE);
                }
                break;
            case R.id.liked_events:
                noEvents.setVisibility(View.GONE);
                eventsRecycler.setVisibility(View.VISIBLE);
                rsvpList.clear();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                for (Event event : list) {
                    if (event.getInterestList().contains(AppController.prefs.getString(Constants.loggedInUserId, ""))) {
                        rsvpList.add(event);
                    }
                }
                if (rsvpList.size() == 0) {
                    noEvents.setVisibility(View.VISIBLE);
                    eventsRecycler.setVisibility(View.GONE);
                }
                eventAdapter.addData(rsvpList);
                break;
            case R.id.log_out:
                FirebaseAuth.getInstance().signOut();

                Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }
}
