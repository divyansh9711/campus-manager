package com.example.divyanshsingh.campusmanager.models;

import android.os.Parcel;
import android.os.Parcelable;
/**
 * Created By Divyansh Singh
 */

public class CreatorInfo implements Parcelable{

    private String createType;  // create-1, modify-2, delete-3
    private String creatorId; // userId
    private String creatorImageUrl;
    private String creatorName;
    //Contact information of creator
    private String email;
    private String mobile;
    private String createDate;

    public CreatorInfo(){}

    protected CreatorInfo(Parcel in) {
        createType = in.readString();
        creatorId = in.readString();
        creatorImageUrl = in.readString();
        creatorName = in.readString();
        email = in.readString();
        mobile = in.readString();
        createDate = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(createType);
        dest.writeString(creatorId);
        dest.writeString(creatorImageUrl);
        dest.writeString(creatorName);
        dest.writeString(email);
        dest.writeString(mobile);
        dest.writeString(createDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CreatorInfo> CREATOR = new Creator<CreatorInfo>() {
        @Override
        public CreatorInfo createFromParcel(Parcel in) {
            return new CreatorInfo(in);
        }

        @Override
        public CreatorInfo[] newArray(int size) {
            return new CreatorInfo[size];
        }
    };

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getCreateType() {
        return createType;
    }

    public void setCreateType(String createType) {
        this.createType = createType;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorImageUrl() {
        return creatorImageUrl;
    }

    public void setCreatorImageUrl(String creatorImageUrl) {
        this.creatorImageUrl = creatorImageUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}
