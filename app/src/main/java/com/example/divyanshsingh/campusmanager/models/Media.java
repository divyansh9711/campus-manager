package com.example.divyanshsingh.campusmanager.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
/**
 * Created By Divyansh Singh
 */

public class Media extends Profile implements Parcelable {

    private String mediaId;
    private String mediaRef;
    private String mediaParentId;
    private Category mediaType;						// logo, profile, cover

    private String mediaUrl;
    private ArrayList<String> mediaUrls;

    private ArrayList<Media> mediaList;

    // Media constants
    public static String Logo = "901";
    public static String Cover = "902";

    // Media Status
    public static String mediaUsed = "3";
    public static String mediaUnused = "4";

    public Media(){
        super();
    }

    protected Media(Parcel in) {
        mediaId = in.readString();
        mediaRef = in.readString();
        mediaParentId = in.readString();
        mediaType = in.readParcelable(Category.class.getClassLoader());
        mediaUrl = in.readString();
        mediaUrls = in.createStringArrayList();
        mediaList = in.createTypedArrayList(Media.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mediaId);
        dest.writeString(mediaRef);
        dest.writeString(mediaParentId);
        dest.writeParcelable(mediaType, flags);
        dest.writeString(mediaUrl);
        dest.writeStringList(mediaUrls);
        dest.writeTypedList(mediaList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Media> CREATOR = new Creator<Media>() {
        @Override
        public Media createFromParcel(Parcel in) {
            return new Media(in);
        }

        @Override
        public Media[] newArray(int size) {
            return new Media[size];
        }
    };

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getMediaRef() {
        return mediaRef;
    }

    public void setMediaRef(String mediaRef) {
        this.mediaRef = mediaRef;
    }

    public String getMediaParentId() {
        return mediaParentId;
    }

    public void setMediaParentId(String mediaParentId) {
        this.mediaParentId = mediaParentId;
    }

    public Category getMediaType() {
        return mediaType;
    }

    public void setMediaType(Category mediaType) {
        this.mediaType = mediaType;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public ArrayList<String> getMediaUrls() {
        return mediaUrls;
    }

    public void setMediaUrls(ArrayList<String> mediaUrls) {
        this.mediaUrls = mediaUrls;
    }

    public ArrayList<Media> getMediaList() {
        return mediaList;
    }

    public void setMediaList(ArrayList<Media> mediaList) {
        this.mediaList = mediaList;
    }
}
