package com.example.divyanshsingh.campusmanager.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.divyanshsingh.campusmanager.R;
import com.example.divyanshsingh.campusmanager.controller.AppController;
import com.example.divyanshsingh.campusmanager.models.Event;
import com.example.divyanshsingh.campusmanager.utils.Constants;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created By Divyansh Singh
 */
public class EventDetailActivity extends AppCompatActivity {

    private Button rsvp;
    View includeLayout;
    private TextView eventName,
            eventDate,
            eventTime,
            eventStartTime,
            eventEndTime,
            eventStartDate,
            eventEndDate,
            eventInfo,
            eventType,
            venue,
            timeLayout,
            dateLayout,
            organizerLayout,
            organizer, atendees;
    private Event event;
    private FirebaseDatabase database;
    boolean flag;
    private DatabaseReference myRef;
    private ConstraintLayout constraintLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        eventName = findViewById(R.id.event_name);
        eventDate = findViewById(R.id.event_day);
        eventTime = findViewById(R.id.event_time);

        eventStartTime = findViewById(R.id.event_start_time);
        eventEndTime = findViewById(R.id.event_end_time);
        eventStartDate = findViewById(R.id.event_start_day);
        eventEndDate = findViewById(R.id.event_end_day);
        includeLayout = findViewById(R.id.to_fro_fields);
        timeLayout = findViewById(R.id.event_time_layout);
        dateLayout = findViewById(R.id.event_day_layout);
        rsvp = findViewById(R.id.add);
        eventInfo = findViewById(R.id.event_info);
        constraintLayout = findViewById(R.id.constraint_layout);
        venue = findViewById(R.id.venue);
        eventType = findViewById(R.id.event_type);
        atendees = findViewById(R.id.atendees);
        organizerLayout = findViewById(R.id.organizerLayout);
        organizer = findViewById(R.id.organizer);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        flag = true;
        rsvp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (String user : event.getInterestList()) {
                    if (user.equals(AppController.prefs.getString(Constants.loggedInUserId, ""))) {
                        flag = false;
                        Toast.makeText(EventDetailActivity.this,"You have already registered yourself as an attendant to this event !",Toast.LENGTH_LONG).show();
                    }
                }
                if (flag) {
                    event.getInterestList().add(AppController.prefs.getString(Constants.loggedInUserId, ""));
                    myRef.child("events").child(event.getCreator().getCreatorId()).child(event.getCreateDateId()).setValue(event);
                }
                rsvp.setVisibility(View.GONE);
            }
        });


        Intent intent = getIntent();
        event = (Event) intent.getExtras().get("EVENT");
        if (event != null) {
            setUpDisplay();
        }

    }

    private void setUpDisplay() {
        if (event.getTiming() != null && event.getTiming().getEndDate().equals("") && event.getTiming().getEndTime().equals("")) {
            eventDate.setVisibility(View.VISIBLE);
            eventTime.setVisibility(View.VISIBLE);
            //timeLayout.setVisibility(View.VISIBLE);
            //includeLayout.setVisibility(View.GONE);
            //dateLayout.setVisibility(View.VISIBLE);
            //ConstraintSet constraintSet = new ConstraintSet();
            //constraintSet.clone(constraintLayout);
            //constraintSet.clear(R.id.event_info_layout,ConstraintSet.TOP);
            //constraintSet.connect(R.id.event_info_layout,ConstraintSet.TOP,R.id.event_time, ConstraintSet.BOTTOM);
            //constraintSet.applyTo(constraintLayout);

            eventTime.setText(event.getTiming().getStartTime());
            eventDate.setText(event.getTiming().getStartDate());

        } else {
            //eventDate.setVisibility(View.GONE);
            //dateLayout.setVisibility(View.GONE);
            //eventTime.setVisibility(View.GONE);
            //timeLayout.setVisibility(View.GONE);
            //includeLayout.setVisibility(View.VISIBLE);
            //ConstraintSet constraintSet = new ConstraintSet();
            //constraintSet.clone(constraintLayout);
            //constraintSet.clear(R.id.event_info_layout,ConstraintSet.TOP);
            //constraintSet.connect(R.id.event_info_layout,ConstraintSet.TOP,includeLayout.getId(), ConstraintSet.BOTTOM);
            //constraintSet.applyTo(constraintLayout);

            eventDate.setText(event.getTiming().getStartDate() + " - " + event.getTiming().getEndDate());
            eventTime.setText(event.getTiming().getStartTime() + " - " + event.getTiming().getEndTime());
        }

        if (event.getTitle() != null) {
            eventName.setText(event.getTitle());
        }
        if (event.getVenue() != null) {
            venue.setText(event.getVenue().getAddressId());
        }
        if (event.getType() != null) {
            eventType.setText(event.getType().getCategoryTitle());
        }
        if (event.getInfo() != null) {
            eventInfo.setText(event.getInfo());
        }
        if (event.getCreator() != null) {
            organizer.setText(event.getCreator().getCreatorName());
        }
        if (event.getInterestList() != null) {
            atendees.setText(String.valueOf(event.getInterestList().size() - 1));
        }
    }
}
