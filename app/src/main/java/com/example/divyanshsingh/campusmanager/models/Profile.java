package com.example.divyanshsingh.campusmanager.models;

import android.os.Parcel;
import android.os.Parcelable;
/**
 * Created By Divyansh Singh
 */

public class Profile implements Parcelable {

    private String id;
    private String parent;
    private CreatorInfo creator;
    private String statusId;

    private String title;
    private String info;
    private String createDateId;
    private Category type;
    private Category category;

    private Media media;

    private Contact contact;

    public Profile(){}

    protected Profile(Parcel in) {
        id = in.readString();
        parent = in.readString();
        creator = in.readParcelable(CreatorInfo.class.getClassLoader());
        statusId = in.readString();
        title = in.readString();
        info = in.readString();
        createDateId = in.readString();
        type = in.readParcelable(Category.class.getClassLoader());
        category = in.readParcelable(Category.class.getClassLoader());
        media = in.readParcelable(Media.class.getClassLoader());
        contact = in.readParcelable(Contact.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(parent);
        dest.writeParcelable(creator, flags);
        dest.writeString(statusId);
        dest.writeString(title);
        dest.writeString(info);
        dest.writeString(createDateId);
        dest.writeParcelable(type, flags);
        dest.writeParcelable(category, flags);
        dest.writeParcelable(media, flags);
        dest.writeParcelable(contact, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Profile> CREATOR = new Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel in) {
            return new Profile(in);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };

    public String getCreateDateId() {
        return createDateId;
    }

    public void setCreateDateId(String createDateId) {
        this.createDateId = createDateId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public CreatorInfo getCreator() {
        return creator;
    }

    public void setCreator(CreatorInfo creator) {
        this.creator = creator;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Category getType() {
        return type;
    }

    public void setType(Category type) {
        this.type = type;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
}
