package com.example.divyanshsingh.campusmanager.models;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
/**
 * Created By Divyansh Singh
 */

@SuppressLint("ParcelCreator")
public class Category extends Profile implements Parcelable,Cloneable {

    private String categoryId;
    private String categoryTitle;
    private String categoryInfo;
    private String categoryType;
    private String categoryParentId;

    private ArrayList<Category> categoryList;

    public Category(){super();}

    protected Category(Parcel in) {
        categoryId = in.readString();
        categoryTitle = in.readString();
        categoryInfo = in.readString();
        categoryType = in.readString();
        categoryParentId = in.readString();
        categoryList = in.createTypedArrayList(Category.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(categoryId);
        dest.writeString(categoryTitle);
        dest.writeString(categoryInfo);
        dest.writeString(categoryType);
        dest.writeString(categoryParentId);
        dest.writeTypedList(categoryList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public String getCategoryInfo() {
        return categoryInfo;
    }

    public void setCategoryInfo(String categoryInfo) {
        this.categoryInfo = categoryInfo;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getCategoryParentId() {
        return categoryParentId;
    }

    public void setCategoryParentId(String categoryParentId) {
        this.categoryParentId = categoryParentId;
    }

    public ArrayList<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(ArrayList<Category> categoryList) {
        this.categoryList = categoryList;
    }
}
