package com.example.divyanshsingh.campusmanager.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.divyanshsingh.campusmanager.R;
import com.example.divyanshsingh.campusmanager.activity.EventDetailActivity;
import com.example.divyanshsingh.campusmanager.controller.AppController;
import com.example.divyanshsingh.campusmanager.models.Event;
import com.example.divyanshsingh.campusmanager.utils.Constants;

import java.util.ArrayList;
import java.util.List;
/**
 * Created By Divyansh Singh
 */
public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventAdapterViewHolder> {

    private List<Event> list;
    private Context context;

    public EventAdapter(Context context, List<Event> list) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public EventAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_item, parent, false);
        return new EventAdapterViewHolder(view);
}

    @Override
    public void onBindViewHolder(@NonNull EventAdapterViewHolder holder, int position) {
        Event item = list.get(position);
        holder.eventName.setText(item.getTitle());
        holder.eventTiming.setText(item.getTiming().getStartTime() + " : " + item.getTiming().getStartDate());
        holder.venue.setText(item.getVenue().getAddressId());
        holder.organizerName.setText(item.getCreator().getCreatorName());
        if(item.getInterestList().contains(AppController.prefs.getString(Constants.loggedInUserId,""))){
            //holder.eventImage.setBackgroundColor(ContextCompat.getColor(context,R.color.google_red));
            holder.eventImage.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public void addData(ArrayList<Event> newList){
        if(newList == null){
            return;
        }
        list = newList;
        notifyDataSetChanged();
    }

    class EventAdapterViewHolder extends RecyclerView.ViewHolder {
        ImageView eventImage;
        TextView eventName;
        TextView eventTiming;
        TextView venue;
        TextView organizerName;
        ConstraintLayout root;

        public EventAdapterViewHolder(View itemView) {
            super(itemView);
            root = itemView.findViewById(R.id.root);
            eventImage = itemView.findViewById(R.id.event_image);
            eventName = itemView.findViewById(R.id.events_name);
            venue = itemView.findViewById(R.id.venue);
            organizerName = itemView.findViewById(R.id.organizer);
            eventTiming = itemView.findViewById(R.id.event_time);

            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, EventDetailActivity.class);
                    intent.putExtra("EVENT",list.get(getAdapterPosition()));
                    context.startActivity(intent);
                }
            });
        }
    }
}
