package com.example.divyanshsingh.campusmanager.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
/**
 * Created By Divyansh Singh
 */

public class Contact extends Profile implements Parcelable {

    private String contactId;
    private String contactParentId;
    private String contactRef;

    // use to store email / mobile along with verification information
    private Category contactType;                    // Entity, Address, Place, Email, Mobile, Aadhar, Pan, Voter, dl, Media, DateTime etc.

    // userId, campus/shopId, email, mobile, pan, voter, dl
    private String contactValue;


    private Boolean isContactVerified;

    private Venue venue;

    private ArrayList<Contact> contactList;

    // Contact constants
    public static String Mobile = "701";
    public static String Email = "702";
    public static String Address = "703";

    public Contact() {
        super();
    }


    protected Contact(Parcel in) {
        contactId = in.readString();
        contactParentId = in.readString();
        contactRef = in.readString();
        contactType = in.readParcelable(Category.class.getClassLoader());
        contactValue = in.readString();
        byte tmpIsContactVerified = in.readByte();
        isContactVerified = tmpIsContactVerified == 0 ? null : tmpIsContactVerified == 1;
        venue = in.readParcelable(Venue.class.getClassLoader());
        contactList = in.createTypedArrayList(Contact.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(contactId);
        dest.writeString(contactParentId);
        dest.writeString(contactRef);
        dest.writeParcelable(contactType, flags);
        dest.writeString(contactValue);
        dest.writeByte((byte) (isContactVerified == null ? 0 : isContactVerified ? 1 : 2));
        dest.writeParcelable(venue, flags);
        dest.writeTypedList(contactList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getContactParentId() {
        return contactParentId;
    }

    public void setContactParentId(String contactParentId) {
        this.contactParentId = contactParentId;
    }

    public String getContactRef() {
        return contactRef;
    }

    public void setContactRef(String contactRef) {
        this.contactRef = contactRef;
    }

    public Category getContactType() {
        return contactType;
    }

    public void setContactType(Category contactType) {
        this.contactType = contactType;
    }

    public String getContactValue() {
        return contactValue;
    }

    public void setContactValue(String contactValue) {
        this.contactValue = contactValue;
    }

    public Boolean getContactVerified() {
        return isContactVerified;
    }

    public void setContactVerified(Boolean contactVerified) {
        isContactVerified = contactVerified;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public ArrayList<Contact> getContactList() {
        return contactList;
    }

    public void setContactList(ArrayList<Contact> contactList) {
        this.contactList = contactList;
    }
}
