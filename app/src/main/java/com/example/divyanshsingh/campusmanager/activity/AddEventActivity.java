package com.example.divyanshsingh.campusmanager.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.divyanshsingh.campusmanager.R;
import com.example.divyanshsingh.campusmanager.controller.AppController;
import com.example.divyanshsingh.campusmanager.models.Category;
import com.example.divyanshsingh.campusmanager.models.CreatorInfo;
import com.example.divyanshsingh.campusmanager.models.Event;
import com.example.divyanshsingh.campusmanager.models.Timing;
import com.example.divyanshsingh.campusmanager.models.Venue;
import com.example.divyanshsingh.campusmanager.utils.Constants;
import com.example.divyanshsingh.campusmanager.utils.DateTimeUtils;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created By Divyansh Singh
 */
public class AddEventActivity extends AppCompatActivity implements View.OnClickListener {

    Button addEvent;
    private ImageView dateFrag, timeFrag, getEnds, arrow1, arrow2;
    View includeLayout;
    private EditText eventName, eventDate, eventTime, eventStartTime, eventEndTime, eventStartDate, eventEndDate, eventInfo, venueLocation, expectedAudience;
    String time, date, dateId;
    int temp = 0;
    private CheckBox sendEmail;
    private TextInputLayout timeLayout;
    RadioButton formal, informal;
    boolean emailSent;
    private Intent emailIntent;
    private TextInputLayout dateLayout, infoLayout;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private ConstraintLayout constraintLayout;
    private Spinner spinner;
    private TextInputLayout endTimeLayout, endDayLayout;
    int startDate = 0;
    int endDate = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);

        dateFrag = findViewById(R.id.date_picker);
        timeFrag = findViewById(R.id.time_picker);
        eventName = findViewById(R.id.event_name);
        eventDate = findViewById(R.id.event_day);
        eventTime = findViewById(R.id.event_time);
        timeLayout = findViewById(R.id.event_time_layout);
        dateLayout = findViewById(R.id.event_day_layout);
        eventStartTime = findViewById(R.id.event_start_time);
        eventEndTime = findViewById(R.id.event_end_time);
        eventStartDate = findViewById(R.id.event_start_day);
        eventEndDate = findViewById(R.id.event_end_day);
        addEvent = findViewById(R.id.add);
        getEnds = findViewById(R.id.get_ends);
        includeLayout = findViewById(R.id.to_fro_fields);
        endDayLayout = findViewById(R.id.event_end_day_layout);
        endTimeLayout = findViewById(R.id.event_end_time_layout);
        eventInfo = findViewById(R.id.event_info);
        infoLayout = findViewById(R.id.event_info_layout);
        constraintLayout = findViewById(R.id.constraint_layout);
        spinner = findViewById(R.id.venue);
        formal = findViewById(R.id.formal_event);
        informal = findViewById(R.id.informal_event);
        venueLocation = findViewById(R.id.venue_location);
        expectedAudience = findViewById(R.id.audience_type);
        sendEmail = findViewById(R.id.send_email);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setUpSpinner();
        //arrow1 = findViewById(R.id.arrow2);
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        //emailIntent.setType("text/plain");

        dateFrag.setOnClickListener(this);
        timeFrag.setOnClickListener(this);
        addEvent.setOnClickListener(this);
        getEnds.setOnClickListener(this);
        eventDate.setOnClickListener(this);
        eventTime.setOnClickListener(this);
        eventEndTime.setOnClickListener(this);
        eventEndDate.setOnClickListener(this);
        eventStartTime.setOnClickListener(this);
        eventStartDate.setOnClickListener(this);
    }

    private void setUpSpinner() {
        @SuppressLint("UseSparseArrays") HashMap<Integer, String> venueMap = new HashMap<>();
        // BH1 - 1, BH2 - 2, BH3 - 3, GH1 - 4, ACAD - 5, SAC - 6, OAT - 7, CP - 8, OTHER - 9
        venueMap.put(1, "Boys Hall-1");
        venueMap.put(2, "Boys Hall-2");
        venueMap.put(3, "Boys Hall-3");
        venueMap.put(4, "Girls Hall");
        venueMap.put(5, "Lecture Hall");
        venueMap.put(6, "SAC");
        venueMap.put(7, "OAT");
        venueMap.put(8, "Central Plaza");
        venueMap.put(9, "Other");
        ArrayList<String> list = new ArrayList<>(venueMap.values());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddEventActivity.this,
                android.R.layout.simple_spinner_item, list);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.date_picker:
                setVisible(true);
                getTime(eventDate);
                break;
            case R.id.time_picker:
                setVisible(true);
                getDate(eventTime);
                break;
            case R.id.add:
                confirmAndAdd();
                if (sendEmail.isChecked()) {
                    startActivity(Intent.createChooser(emailIntent, "Select"));
                }
                if (!checkFields()) {
                    finish();
                }

                break;
            case R.id.get_ends:
                setVisible(false);
                temp = 1;
                eventStartTime.setText(eventTime.getText().toString());
                eventStartDate.setText(eventDate.getText().toString());
                break;
            case R.id.event_end_time:
                getDate(eventEndTime);
                break;
            case R.id.event_end_day:
                getTime(eventEndDate);
                break;
            case R.id.event_start_time:
                getDate(eventStartTime);
                break;
            case R.id.event_start_day:
                getTime(eventStartDate);
                break;
            case R.id.event_day:
                getTime(eventDate);
                break;
            case R.id.event_time:
                getDate(eventTime);
                break;

        }
    }

    private void confirmAndAdd() {
        String uId = "";
        if (checkFields()) {
            return;
        }
        Event event = new Event();
        event.setTitle(eventName.getText().toString());
        Timing timing = new Timing();
        timing.setStartDate(eventDate.getText().toString());
        timing.setStartTime(eventTime.getText().toString());
        if (eventDate.getText().toString().equals("")) {
            timing.setStartDate(eventStartDate.getText().toString());
        }
        if (eventTime.getText().toString().equals("")) {
            timing.setStartTime(eventStartTime.getText().toString());
        }
        timing.setEndDate(eventEndDate.getText().toString());
        timing.setEndTime(eventEndTime.getText().toString());


        uId = AppController.prefs.getString(Constants.loggedInUserId, "");

        CreatorInfo creatorInfo = new CreatorInfo();

        date = DateTimeUtils.getCurrentFormattedDate();

        creatorInfo.setCreatorId(AppController.prefs.getString(Constants.loggedInUserId, ""));
        creatorInfo.setEmail(AppController.prefs.getString(Constants.loggedInEmail, ""));
        creatorInfo.setCreatorName(AppController.prefs.getString(Constants.loggedInUserName, ""));
        creatorInfo.setCreateDate(date);
        Category category = new Category();
        if (formal.isChecked()) {
            category.setCategoryId("0");
            category.setCategoryTitle("formal");

        } else {
            category.setCategoryId("1");
            category.setCategoryTitle("Informal");
        }
        event.setType(category);
        event.setCreateDateId(dateId);
        event.setTiming(timing);
        event.setCreator(creatorInfo);
        Venue venue = new Venue();
        venue.setVenueId(String.valueOf(spinner.getSelectedItemPosition()));
        venue.setCreatedDate(DateTimeUtils.getCurrentFormattedDate());
        venue.setCreatedBy(AppController.prefs.getString(Constants.loggedInUserId, ""));
        venue.setAddressId(spinner.getSelectedItem() + " " + venueLocation.getText().toString());
        event.setVenue(venue);
        ArrayList<String> array = new ArrayList<>();
        array.add("temp");
        event.setInterestList(array);
        event.setInfo(eventInfo.getText().toString());
        String body = "\n" + event.getInfo() + "\n\n" + "Date: " + event.getTiming().getStartDate() + "\n" + "Timing: " + event.getTiming().getStartTime()
                + "\n\n" + "Regards," + "\n" + event.getCreator().getCreatorName();
        Uri uri = Uri.parse("mailto:" + event.getCreator().getEmail())
                .buildUpon()
                .appendQueryParameter("subject", event.getTitle())
                .appendQueryParameter("body", body).build();
        emailIntent = new Intent(Intent.ACTION_SENDTO, uri);

        if (!uId.equals("")) {
            myRef.child("events").child(uId).child(dateId).setValue(event);
        }


    }

    private boolean checkFields() {

        if (eventDate == null || eventTime == null || eventName == null) {
            Toast.makeText(AddEventActivity.this, "Something went wrong please try again !", Toast.LENGTH_LONG).show();
            return true;
        } else if (eventName.getText().toString().equals("")) {
            Toast.makeText(AddEventActivity.this, "Your Event must have name !!", Toast.LENGTH_LONG).show();
            return true;
        } else if (eventDate.getText().toString().equals("") && eventStartDate.getText().toString().equals("")) {
            Toast.makeText(AddEventActivity.this, "You must tell everyone when your event starts.", Toast.LENGTH_LONG).show();
            return true;
        } else if (eventTime.getText().toString().equals("") && eventStartTime.getText().toString().equals("")) {
            Toast.makeText(AddEventActivity.this, "Tell us the time when event starts.", Toast.LENGTH_LONG).show();
            return true;
        } else if (!eventStartDate.getText().toString().equals("") && !eventEndDate.getText().toString().equals("")) {
            startDate = Integer.parseInt(DateTimeUtils.returnReverse(eventStartDate.getText().toString()));
            endDate = Integer.parseInt(DateTimeUtils.returnReverse(eventEndDate.getText().toString()));
            if (startDate > endDate) {
                Toast.makeText(AddEventActivity.this, "We haven't invented time travel yet !! Your event must start BEFORE it ends.", Toast.LENGTH_LONG).show();
                return true;
            }
        } else if (!formal.isChecked() && !informal.isChecked()) {
            Toast.makeText(AddEventActivity.this, "Please tell us weather your event is formal or not !", Toast.LENGTH_LONG).show();
            return true;
        } else if (venueLocation.getText().toString().equals("")) {
            Toast.makeText(AddEventActivity.this, "You must tell use the venue of your event !", Toast.LENGTH_LONG).show();
            return true;
        } else if (eventInfo.getText().toString().equals("")) {
            Toast.makeText(AddEventActivity.this, "Please provide information about the event", Toast.LENGTH_LONG).show();
            return true;
        }
        return false;
    }

    public void getTime(final EditText view) {
        date = "";
        DatePickerDialog datePickerDialog = new DatePickerDialog(AddEventActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int yy, int mm, int dd) {
                String m = "", d = "";
                if (view.getId() == R.id.event_start_day || view.getId() == R.id.event_day || view.getId() == R.id.event_end_day) {
                    m = String.valueOf(mm + 1);
                    d = String.valueOf(dd);
                    if (mm < 9) {
                        m = "0" + String.valueOf(mm + 1);
                    }
                    if (dd < 10) {
                        d = "0" + String.valueOf(dd);
                    }
                    dateId = String.valueOf(yy) + m + d + DateTimeUtils.getHour() + DateTimeUtils.getMinute() + DateTimeUtils.getSecond();
                }
                date = d + "/" + m + "/" + String.valueOf(yy);
                view.setText(date);
            }
        }, DateTimeUtils.getYear(0), DateTimeUtils.getMonth(0), DateTimeUtils.getDay(0));
        datePickerDialog.setTitle("Day");
        datePickerDialog.show();
    }

    public void getDate(final EditText view) {
        time = "";
        TimePickerDialog timePickerDialog = new TimePickerDialog(AddEventActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                if (i > 12) {
                    time = String.valueOf(i - 12) + ":" + String.valueOf(i1) + " PM";
                } else {
                    time = String.valueOf(i) + ":" + String.valueOf(i1) + " AM";
                }

                view.setText(time);
            }
        }, 0, 0, true);
        timePickerDialog.setTitle("Time");
        timePickerDialog.show();
    }

    public void setVisible(boolean flag) {
        if (flag) {
            eventDate.setVisibility(View.VISIBLE);
            dateLayout.setVisibility(View.VISIBLE);
            eventTime.setVisibility(View.VISIBLE);
            timeLayout.setVisibility(View.VISIBLE);
            getEnds.setVisibility(View.VISIBLE);
            includeLayout.setVisibility(View.GONE);

            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(constraintLayout);
            constraintSet.clear(R.id.event_info_layout, ConstraintSet.TOP);
            constraintSet.connect(R.id.event_info_layout, ConstraintSet.TOP, R.id.event_day_layout, ConstraintSet.BOTTOM);
            constraintSet.applyTo(constraintLayout);

        } else {
            eventDate.setVisibility(View.GONE);
            dateLayout.setVisibility(View.GONE);
            eventTime.setVisibility(View.GONE);
            timeLayout.setVisibility(View.GONE);
            getEnds.setVisibility(View.GONE);
            includeLayout.setVisibility(View.VISIBLE);

            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(constraintLayout);
            constraintSet.clear(R.id.event_info_layout, ConstraintSet.TOP);
            constraintSet.connect(R.id.event_info_layout, ConstraintSet.TOP, includeLayout.getId(), ConstraintSet.BOTTOM);

            constraintSet.applyTo(constraintLayout);

        }
    }
}
